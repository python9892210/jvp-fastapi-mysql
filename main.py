from fastapi import FastAPI,Query,Body,Form, File,UploadFile
from pydantic import BaseModel,Field
from typing import Optional, Annotated
from datetime import datetime, time, timedelta
from uuid import UUID

app=FastAPI()

@app.post("/auth/login")
async def login():
    return  {"message":"hello world"}

@app.post("/auth/register")
async def register():
    return  {"message":"hello world"}
    

class Items(BaseModel):
    name:str
    description:Optional[str]=None
    price:float
    tax:float|None=None

@app.post("/items")
async def create(item:Items):
    item_dict=item.dict()
    if item.tax:
        price_with_tax=item.price+item.tax
        item_dict.update({"price_with_tax":price_with_tax})
    return  item_dict;    

@app.put("/items/{item_id}")
async def update(item_id:str,item:Items):
    return  {"item_id":item_id,**item.dict()};    

@app.get("/items")
async def get(q:str|None =Query(None,min_length=3,max_length=9)):
    results=[{"name":"foo"},{"name":"bar"}]
    if q:
        results.update({"q":q})
    return  results    


class Product(BaseModel):
    name:str
    description:str | None=Field(
        None,title="title string",max_lenght=300
    )
    price :float= Field(..., gt=0, description="price must be greater than zero")
    tax:float | None
    
@app.post("/products")
async def create(product:Product=Body(...,embed=True)):
    return  {"product":product}    

class Transcation(BaseModel):
    name:str
    price:float
    tax:float|None
    items:list[Items]
    
@app.post("/transactions")
async def create(transaction:Transcation=Body(...,embed=True)):
    return  {"transaction":transaction}   

@app.post("/blash")
async def create(blash:dict[int,float]):
    return  {"blash":blash}  


class Roles(BaseModel):
    id:int
    name:str

    class Config:
        json_schema_extra={
            "example":{
                "id":1,
                "name":"Admin"
            }
        }

@app.post("/roles")
async def create(roles:Roles):
    return  roles;  

class Category(BaseModel):
    id:int =Field(...,example=1)
    name:str =Field(...,example="Book")

@app.post("/category")
async def create(category:Category):
    return  category;   

class Address(BaseModel):
    id:int
    name:str
    phone:str
    city:str
    region:str
    state:str
    
@app.post("/address")
async def create(address:Address=Body(
    ...,
    example={
        "id":"1",
        "name":"vila mashroom",
        "phone":"123445676789",
        "city":"Bandung",
        "province":"west java",
        "state":"indonesia"
    }
)):
    return  address;    


@app.post("/menus")
async def read_items(
    item_id: UUID,
    start_datetime: Annotated[datetime, Body()],
    end_datetime: Annotated[datetime, Body()],
    process_after: Annotated[timedelta, Body()],
    repeat_at: Annotated[time | None, Body()] = None,
):
    start_process = start_datetime + process_after
    duration = end_datetime - start_process
    return {
        "id": "1",
        "start_datetime": start_datetime,
        "end_datetime": end_datetime,
        "process_after": process_after,
        "repeat_at": repeat_at,
        "start_process": start_process,
        "duration": duration,
    }
    
class User(BaseModel):
    id:int =Field(...,example=1)
    name:str =Field(...,example="Nurdin")
    email:str =Field(...,example="nurdin@gmail.com")
    password:str =Field()

@app.post("/auth/login")
async def login(username: Annotated[str, Form()], password: Annotated[str, Form()]):
    return {"username": username}  

@app.post("/file")
async def create_file(file: Annotated[bytes, File()]):
    return {"file_size": len(file)}


@app.post("/uploadfile")
async def create_upload_file(file: UploadFile):
    return {"filename": file.filename}

@app.post("/files")
async def create_file(file: Annotated[list[bytes], File()]):
    return {"file_size": len(file)}


@app.post("/uploadfiles")
async def create_upload_file(file: list[UploadFile]):
    return {"filename": file.filename}